package nl.bioinf.ahclugtenberg.gff_query.CLI;

import nl.bioinf.ahclugtenberg.gff_query.IO.GffParser;
import nl.bioinf.ahclugtenberg.gff_query.IO.SummaryCreator;
import org.apache.commons.cli.*;

import java.io.FileNotFoundException;

/**
 * Created by Anouk Lugtenberg on 10-11-2017.
 * Controls the command line options which the user has given
 * Starts corresponding methods for certain options
 */
public class ApacheCLiOptionsProvider{
    private static final String HELP = "help";
    private static final String SUMMARY = "summary";
    private static final String INFILE = "infile";
    private static final String FETCH_TYPE = "fetch_type";
    private static final String FETCH_REGION = "fetch_region";
    private static final String FILTER = "filter";
    private static final String FETCH_CHILDREN = "fetch_children";
    private static final String FIND_WILDCARD = "find_wildcard";
    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * constructs with the command line array
     * @param args the CL array
     */
    public ApacheCLiOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true
     *
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option inFileOption = new Option("f", INFILE, true, "The GffObject file to be used");
        Option summaryOption = new Option("s", SUMMARY, false, "Prints summary");
        Option fetchTypeOption = new Option("ft", FETCH_TYPE, true, "Will fetch a certain type of a feature (e.g. gene, CDS");
        Option fetchRegionOption = new Option("fr", FETCH_REGION, true, "Will select all features that are included completely within the given coordinates");
        Option filterOption = new Option("filter", FILTER, true, "<SOURCE, SCORE, ORIENTATION MAXIMUM AND/OR MINIMUM LENGTH> Will" +
                "filter any of these data fields, in any combination:\n" +
                "SOURCE: should filter on source attribute\n" +
                "SCORE: should filter on score attribute\n" +
                "ORIENTATION: should be defined using a '+', '-' or '.' character\n" +
                "MINIMUM LENGTH: should be defined using an integer\n" +
                "MAXIMUM LENGTH: should be defined using an integer\n" +
                "The filter should be specified using the format 'source|score|orientation|maximum_length|minimum_length', where suppression" +
                "of an individual filter is indicated using an asterisk (*)");
        Option fetchChildrenOption = new Option("fc", FETCH_CHILDREN, true, "Returns the GFF3-types that have the given parent ID as ancestor (parent" +
                "or parents)");
        Option findWildcardOption = new Option ("fw", FIND_WILDCARD, true, "Lists all the features that have the given wildcard string" +
                "(a regex string) in the Name attribute");
        options.addOption(helpOption);
        options.addOption(inFileOption);
        options.addOption(summaryOption);
        options.addOption(fetchTypeOption);
        options.addOption(fetchRegionOption);
        options.addOption(filterOption);
        options.addOption(fetchChildrenOption);
        options.addOption(findWildcardOption);
    }

    /**
     * Processes the command line arguments
     */
    private void processCommandLine() {
        String inFile;
        String fetchType = null;
        String fetchRegion = null;
        String filter = null;
        String fetchChildren = null;
        String findWildcard = null;

        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (!commandLine.hasOption(INFILE)) {
                throw new FileNotFoundException();
            } else {
                inFile = commandLine.getOptionValue(INFILE);
            }

            //Starts summary if summary option is given by the user
            if (commandLine.hasOption(SUMMARY)) {
                SummaryCreator sc = new SummaryCreator(inFile);
                sc.start();
            }

            if (commandLine.hasOption(FETCH_TYPE)) {
                fetchType = commandLine.getOptionValue(FETCH_TYPE);
            }
            if (commandLine.hasOption(FETCH_REGION)) {
                fetchRegion = commandLine.getOptionValue(FETCH_REGION);
            }
            if (commandLine.hasOption(FILTER)) {
                filter = commandLine.getOptionValue(FILTER);
            }
            if (commandLine.hasOption(FETCH_CHILDREN)) {
                fetchChildren = commandLine.getOptionValue(FETCH_CHILDREN);
            }
            if (commandLine.hasOption(FIND_WILDCARD)) {
                findWildcard = commandLine.getOptionValue(FIND_WILDCARD);
            }

            //Starts parser if any of the options are given by the user
            if (fetchType != null | fetchRegion != null | filter != null | fetchChildren != null | findWildcard != null) {
                GffParser gp = new GffParser(inFile, fetchType, fetchRegion, filter, fetchChildren, findWildcard);
                gp.start();
            }

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        } catch (FileNotFoundException ex) {
            System.out.println("Please provide program with a Gff file. Exiting...");
        }
    }

    /**
     * Prints out the help of the program to the user
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("GffQuery", options);
    }
}
