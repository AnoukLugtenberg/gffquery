package nl.bioinf.ahclugtenberg.gff_query.IO;

import nl.bioinf.ahclugtenberg.gff_query.control.GffObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Anouk Lugtenberg on 10-11-2017.
 * Accepts a gff3 file and makes GffObjects of it lines
 */
class GffFileHandler {
    private String inFile;
    private ArrayList<GffObject> listWithGffObjects = new ArrayList<>();

    /**
     * Takes a gff3 file
     * @param inFile the gff3 file to be used
     */
    GffFileHandler (String inFile) {
        this.inFile = inFile;
        readFile();
    }

    /**
     * Reads the input file given by the user
     * If something goes wrong while parsing, throws an ArrayIndexOutOfBoundsException
     */
    private void readFile() {
        String thisLine;
        int lineNumber = 0;
        try {
            FileReader in = new FileReader(inFile);
            BufferedReader br = new BufferedReader(in);

            while ((thisLine = br.readLine()) != null) {
                lineNumber++;
                try {
                    createGffObject(thisLine);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    System.out.println("Please provide program with correct formatted gff3 file.\n" +
                            "Parsing went wrong on line " + lineNumber + ": " + thisLine);

                }
            }
            in.close();
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Creates gffObjects from lines from the input file
     * @param line a line from the gff3 file
     */
    private void createGffObject(String line) {
        String[] splittedLine;
        GffObject go = new GffObject();
        //If file is tab-separated, split on tab, otherwise split on space
        if (line.contains("\t")) {
            splittedLine = line.split("\t");
        } else {
            splittedLine = line.split(" +");
        }

        go.setSeqname(splittedLine[0]);
        go.setSource(splittedLine[1]);
        go.setFeature(splittedLine[2]);
        go.setStart(Integer.parseInt(splittedLine[3]));
        go.setEnd(Integer.parseInt(splittedLine[4]));
        go.setScore(splittedLine[5]);
        go.setStrand(splittedLine[6]);
        go.setFrame(splittedLine[7]);
        go.setGroupingAttributes(splittedLine[8]);

        listWithGffObjects.add(go);
    }

    ArrayList getGffObjects() {
        return listWithGffObjects;
    }
}
