package nl.bioinf.ahclugtenberg.gff_query.IO;

import nl.bioinf.ahclugtenberg.gff_query.control.GffObject;

import java.io.File;
import java.util.*;

/**
 * Created by Anouk Lugtenberg on 11-11-2017.
 * Creates a summary of a .gff3 file containing:
 * file [filename]
 * molecules with features [names of the sequence]
 * number of features [amount of features]
 * feature counts [amount per feature]
 */
public class SummaryCreator {
    private String inFile;
    private ArrayList<GffObject> gffObjects;
    private String fileName;
    private String moleculesWithFeatures;
    private String moleculeFeatureCounts;

    /**
     * @param inFile the gff3 file to be used to create a summary
     */
    public SummaryCreator(String inFile) {
        this.inFile = inFile;
    }

    /**
     * Starts the creating of the summary
     */
    public void start() {
        GffFileHandler gfh = new GffFileHandler(this.inFile);
        this.gffObjects = gfh.getGffObjects();
        this.fileName = getFileName();
        this.moleculesWithFeatures = buildStringFeatures(getMoleculesWithFeatures());
        this.moleculeFeatureCounts = buildStringCounts(getFeatureCounts());
        printSummary();
    }

    /**
     * Extracts the filename of the filepath
     * @return String with the filename
     */
    private String getFileName() {
        File f = new File(this.inFile);
        return f.getName();
    }

    /**
     * Finds all the different names of the sequence and adds these to a list
     * @return ArrayList containing the different names
     */
    private ArrayList<String> getMoleculesWithFeatures() {
        ArrayList<String> moleculesWithFeatures = new ArrayList<>();
        for (GffObject i : this.gffObjects) {
            if (!moleculesWithFeatures.contains(i.getSeqname())) {
                moleculesWithFeatures.add(i.getSeqname());
            }
        }
        return moleculesWithFeatures;
    }

    /**
     * Counts the the amount of features in gff file
     * @return HashMap containing feature and corresponding amount
     */
    private HashMap<String, Integer> getFeatureCounts() {
        ArrayList<String> gffFeatures = new ArrayList<>();
        HashMap<String, Integer> moleculesFeatureCounts = new HashMap<>();
        for (GffObject i : this.gffObjects) {
            gffFeatures.add(i.getFeature());
        }

        Set<String> unique = new HashSet<>(gffFeatures);
        for (String key : unique) {
            moleculesFeatureCounts.put(key, Collections.frequency(gffFeatures, key));
        }
        return moleculesFeatureCounts;
    }

    /**
     * Builds the string of features to be printed to the user
     * @param list ArrayList containing one or multiple Strings
     * @return String for output to the user
     */
    private String buildStringFeatures(ArrayList<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String value : list) {
            builder.append(value).append(" ");
        }
        return builder.toString();
    }

    /**
     * builds the string for output to the user
     * @param list HashMap containing String and Integer
     * @return String for output to the user
     */
    private String buildStringCounts(HashMap<String, Integer> list) {
        StringBuilder builder = new StringBuilder();
        for (String key : list.keySet())  {
            String value = list.get(key).toString();
            builder.append(String.format("\t%-20s%s%n", key, value));
        }
        return builder.toString();
    }

    /**
     * prints out the summary to the console
     */
    private void printSummary() {
        System.out.println("file\t\t\t\t\t\t" + this.fileName + "\n" +
                "molecules with features\t\t" + this.moleculesWithFeatures + "\n" +
                "number of features\t\t\t" + this.gffObjects.size() + "\n" +
                "feature counts:\n" + this.moleculeFeatureCounts
        );
    }
}
