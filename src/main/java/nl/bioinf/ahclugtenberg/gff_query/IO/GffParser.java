package nl.bioinf.ahclugtenberg.gff_query.IO;

import nl.bioinf.ahclugtenberg.gff_query.control.GffObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Created by Anouk Lugtenberg on 13-11-2017.
 * Parses the gff file and filters on the options given by the user
 * Prints the found gff objects to the user
 */
public class GffParser {
    private String inFile;
    private String fetchType;
    private String fetchRegion;
    private String filter;
    private String fetchChildren;
    private String findWildcard;
    private int start;
    private int stop;
    private ArrayList<GffObject> gffObjects;

    public GffParser (String inFile, String fetchType, String fetchRegion, String filter, String fetchChildren, String findWildcard) {
        this.inFile = inFile;
        this.fetchType = fetchType;
        this.fetchRegion = fetchRegion;
        this.filter = filter;
        this.fetchChildren = fetchChildren;
        this.findWildcard = findWildcard;
    }

    /**
     * Starts the filter process
     * Checks which options are given by the user, and if they're given starts the corresponding method
     * Prints the output to the user
     */
    public void start() {
        GffFileHandler gfh = new GffFileHandler(this.inFile);
        this.gffObjects = gfh.getGffObjects();
        if (this.fetchType != null) {
            fetchFeaturesWithType();
        }

        if (this.fetchRegion != null) {
            checkInputFetchRegion();
            fetchFeaturesWithRegion();
        }

        if (this.filter != null) {
            checkInputFilter();
        }

        if (this.findWildcard != null) {
            findWildcard();
        }

        if (this.fetchChildren != null) {
            fetchChildren(this.fetchChildren);
        }

        //Prints gffobjects from list if any of the options except fetchchildren is given
        //Because fetchchildren uses a different list
        if (this.fetchType != null | this.fetchRegion != null | this.filter != null | this.findWildcard != null) {
            for (GffObject i : this.gffObjects) {
                System.out.println(i);
            }
        }
    }

    /**
     * Fetches the features with a certain type given by the user
     * Removes features from the list who don't have this certain type
     */
    private void fetchFeaturesWithType() {
        this.gffObjects.removeIf((GffObject gff) -> !gff.getFeature().equals(fetchType));
    }

    /**
     * Fetches features within certain region
     */
    private void fetchFeaturesWithRegion() {
        String[] startAndStop = this.fetchRegion.split("\\.\\.");
        this.start = Integer.parseInt(startAndStop[0]);
        this.stop = Integer.parseInt(startAndStop[1]);
        this.gffObjects.removeIf((GffObject gff) -> gff.getStart() <= start || gff.getEnd() >= stop);
    }

    /**
     * Checks if user has given the right input format for the fetching of regions
     * Throws IllegalArgumentException if user input is not in the right format
     */
    private void checkInputFetchRegion() {
        try {
            //First check if input only contains numbers and the separator '..'
            String firstPattern = "[^\\d.]";
            Pattern r = Pattern.compile(firstPattern);
            Matcher m = r.matcher(this.fetchRegion);
            //Throws exception if anything besides numbers or '.' is found in user input
            if (m.find()) {
                throw new IllegalArgumentException();
            }

            //Check if input is in the right format
            String secondPattern = "(\\d+)(\\.{2})(\\d+)";
            Pattern rightFormatPattern = Pattern.compile(secondPattern);
            Matcher rightFormatMatcher = rightFormatPattern.matcher(this.fetchRegion);

            if (!rightFormatMatcher.find()) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Please provide program with the according syntax for fetching regions: --fetch_region [START]..[END]");
        }
    }

    /**
     * Checks if the given input for filter by the user is in the correct format
     * If not, throws an Exception
     */
    private void checkInputFilter() {
        String pattern = "([\\w*]*)\\|([\\d.*]*)\\|([+-.*])\\|([\\d*]*)\\|([\\d*]*)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(this.filter);
        try {
            if (m.find()) {
                String source = m.group(1);
                String score = m.group(2);
                String orientation = m.group(3);
                String minLength = m.group(4);
                String maxLength = m.group(5);
                filter(source, score, orientation, minLength, maxLength);
            } else {
                throw new Exception();
            }
        } catch (Exception ex) {
            System.out.println("Please provide program with according syntax for filter: --filter SOURCE|SCORE|ORIENTATION|MINIMUM LENGTH|MAXIMUM LENGTH");
        }
    }

    /**
     * Checks which options are given by the user for filter, if option is given, corresponding filter method is used
     * @param source on which source attribute should be filtered
     * @param score on which score attribute should be filtered
     * @param orientation on which orientation attribute should be filtered
     * @param minLength the minimum length of nucleotides to be filtered on
     * @param maxLength the maximum length of nucleotides to be filtered on
     */
    private void filter(String source, String score, String orientation, String minLength, String maxLength) {
        if (!source.contains("*")) {
            filterSource(source);
        }
        if (!score.contains("*")) {
            filterScore(score);
        }
        if (!orientation.contains("*")) {
            filterOrientation(orientation);
        }
        if (!minLength.contains("*")) {
            filterLength(minLength, true);
        }
        if (!maxLength.contains("*")) {
            filterLength(maxLength, false);
        }
    }

    /**
     * Filters sequences which do not require for the minimum or maximum length
     * @param length the minimum or maximum length
     * @param minimumLength if it's true, will filter on minimum length, otherwise on maximum length
     */
    private void filterLength(String length, boolean minimumLength) {
        int lengthToBeFiltered = Integer.parseInt(length);
        if (minimumLength) {
            this.gffObjects.removeIf((GffObject gff) -> (gff.getEnd() - gff.getStart()) < lengthToBeFiltered);
        } else {
            this.gffObjects.removeIf((GffObject gff) -> (gff.getEnd() - gff.getStart()) > lengthToBeFiltered);
        }
    }

    /**
     * Filters on source attribute
     * @param source the source attribute
     */
    private void filterSource(String source) {
        this.gffObjects.removeIf((GffObject gff) -> !gff.getSource().equals(source));
    }

    /**
     * Filters on score attribute
     * @param score the score attribute
     */
    private void filterScore(String score) {
        this.gffObjects.removeIf((GffObject gff) -> !gff.getScore().equals(score));
    }

    /**
     * Filters on orientation
     * @param orientation the orientation attribute
     */
    private void filterOrientation(String orientation) {
        this.gffObjects.removeIf((GffObject gff) -> !gff.getStrand().equals(orientation));
    }

    /**
     * Finds all the objects who have a certain wildcard (given as regex) in their name attribute
     * Removes object from the list if wildcard is not found
     */
    private void findWildcard() {
        for (Iterator<GffObject> i = this.gffObjects.iterator(); i.hasNext(); ) {
            GffObject aGffObject = i.next();
            String groupingAttribute = aGffObject.getGroupingAttributes();
            String pattern = this.findWildcard;
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(groupingAttribute);
            if (!m.find()) {
                i.remove();
            }
        }
    }

    /**
     * Fetches the children from a certain parentID
     * @param parentID the parentID from which the children are
     */
    private void fetchChildren(String parentID) {
        ArrayList<GffObject> foundChildren = new ArrayList<>();
        for (GffObject i : this.gffObjects) {
            String groupingAttributes = i.getGroupingAttributes();
            String pattern = "Parent=[^;]*";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(groupingAttributes);
            if (m.find()) {
                Pattern parentPattern = Pattern.compile(parentID);
                Matcher parentMatcher = parentPattern.matcher(m.group(0));
                if (parentMatcher.find()) {
                    foundChildren.add(i);
                }
            }
        }
        for (GffObject i : foundChildren) {
            System.out.println(i);
        }
    }
}
