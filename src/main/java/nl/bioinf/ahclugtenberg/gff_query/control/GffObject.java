package nl.bioinf.ahclugtenberg.gff_query.control;

/**
 * Created by Anouk Lugtenberg on 11-11-2017.
 * Creates GffObjects from gff file
 */

public class GffObject {
    private String seqname;
    private String source;
    private String feature;
    private int start;
    private int end;
    private String score;
    private String strand;
    private String frame;
    private String groupingAttributes;

    public String getSeqname() {
        return seqname;
    }

    public void setSeqname(String seqname) {
        this.seqname = seqname;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getStrand() {
        return strand;
    }

    public void setStrand(String strand) {
        this.strand = strand;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public void setGroupingAttributes(String groupingAttributes) {
        this.groupingAttributes = groupingAttributes;
    }
    public String getGroupingAttributes() { return groupingAttributes; }

    @Override
    public String toString() {
        return this.seqname + "\t" + this.source + "\t" + this.feature + "\t" + this.start +
                "\t" + this.end + "\t" + this.score + "\t" + this.strand + "\t" + this.frame +
                "\t" + this.groupingAttributes;
    }
}
