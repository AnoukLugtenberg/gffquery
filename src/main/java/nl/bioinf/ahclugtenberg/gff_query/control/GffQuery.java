package nl.bioinf.ahclugtenberg.gff_query.control;

import nl.bioinf.ahclugtenberg.gff_query.CLI.ApacheCLiOptionsProvider;

import java.util.Arrays;

/**
 * Created by ahclugtenberg on 18-10-17.
 * Starts the gff3 parser program
 */
public class GffQuery {
    public static void main(String[] args) {
        try {
            ApacheCLiOptionsProvider op = new ApacheCLiOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
            }
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
            + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed. Reason: " + ex.getMessage());
            ApacheCLiOptionsProvider op = new ApacheCLiOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
