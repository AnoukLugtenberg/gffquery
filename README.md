# README #

### What is this repository for? ###

This repository is used to handle Gff3 files as a commandline tool. There are several options which the user can choose from:  
--help shows informative help/usage information  
--infile path to the gff3 inputfile to be used  
--summary creates a textual summary of the parsed file: names of molecules with annotations, types and count of features  
--fetch_type <TYPE> will fetch a certain type of feature (e.g. gene, CDS)  
--fetch_region <COORDINATES> will select all features that are included completely within the given coordinates  
--filter <SOURCE, SCORE, ORIENTATION, MAXIMUM AND/OR MINIMUM LENGTH> will filter on any of these data fields, in any combination. The filter should be specified using the format "source|score|orientation|maximum_length|minimum_length", where suppresion of an individual filter is indicated using an asterisk (*).  
--find_wildcard <WILDCARD STRING> will fetch all the features that have the given wildcard string (a regex string) in the name attribute  
--fetch_children <PARENT ID> will fetch all all the features that have the given parent ID as ancestor  
  (!fetch children is for now only working for direct parents, and not for parents parents)

For more info about the code you can take a view at the javadoc, found in the directory javadoc/

### How do I get set up? ###
To use the program you can use the GffQuery-Fatjar file found in /build/libs, just execute it on the commandline:  
```java -jar GffQuery-Fatjar-1.0.jar <OPTIONS>```  
Some testdata is found in the directory data/ or in the download map of this repository.  

### Who do I talk to? ###

Anouk Lugtenberg  
E-mail: a.h.c.lugtenberg@st.hanze.nl